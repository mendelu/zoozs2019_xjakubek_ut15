#include <iostream>

using namespace std;

class Package {
public:
    string m_address;
    float m_weight;
    string m_email;
    string m_phone;

    Package(string address) {
        m_address = address;
        m_weight = 0.0;
        m_email = "";
        m_phone = "";
    }

    Package(string address, float weight) {
        m_address = address;
        setWeight(weight);
        m_email = "";
        m_phone = "";
    }

    Package(string address, float weight, string email, string phone) {
        m_address = address;
        setWeight(weight);
        m_email = email;
        m_phone = phone;
    }

    Package(string address, float weight, string email) {
        m_address = address;
        setWeight(weight);
        m_email = email;
        m_phone = "";
    }

    void printInfo() {
        cout << "Address: " << m_address << endl;
        cout << "Email: " << m_email << endl;
        cout << "Phone: " << m_phone << endl;
        cout << "Weight: " << m_weight << endl;
    }

    // kontrola vahy
    void setWeight(float weight) {
        if (weight > 0.0) {
            // vaha je vacsia ako 0.0
            m_weight = weight;
        } else {
            // vaha je 0.0 alebo menej
            cout << "Sorry, you must have weight larger than 0!!!!" << endl;
            m_weight = 0.0;
        }
    }
};

int main() {
    Package *package1;
    package1 = new Package("Brno 60200");
    package1->printInfo();

    Package *package2;
    package2 = new Package("Brno 55555", -10);
    package2->printInfo();

    Package *package3;
    package3 = new Package("Brno 55555", 10, "email@gmail.com", "+4205552584");
    package3->printInfo();


    return 0;
}