#include <iostream>

using namespace std;

class ErrorLog {
    static ErrorLog *s_log;
    string m_errors;
public:
    static ErrorLog *getErrorLog() {
        if (s_log == nullptr) {
            s_log = new ErrorLog();
        }
        return s_log;
    }

    void logError(string where, string what) {
        m_errors += "- " + where + ": " + what + "\n";
    }

    string getErrors() {
        return m_errors;
    }

private:
    ErrorLog() {
        m_errors = "Error log: \n";
    }
};

ErrorLog *ErrorLog::s_log = nullptr;

class Dragon {
    float m_lifes;
    float m_power;
    float m_defense;
public:
    Dragon(float power, float defense) {
        setPower(power);
        setDefense(defense);
        m_lifes = 100;
    }

    float getLifes() {
        return m_lifes;
    }

    float getPower() {
        return m_power;
    }

    float getDefense() {
        return m_defense;
    }

    void reduceLifes(float count) {
        m_lifes -= count;
    }


private:
    void setPower(float power) {
        if ((power >= 0) and (power <= 100)) {
            m_power = power;
        } else {
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Dragon::setPower", "Power is out of range "
                                              "<0,100>. Set to 0.");
            m_power = 0;
        }
    }

    void setDefense(float def) {
        if ((def >= 0) and (def <= 200)) {
            m_defense = def;
        } else {
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Dragon::setDefense", "Defense is out of range "
                                                "<0,200>. Set to 0.");
            m_defense = 0;
        }
    }
};

class Knight {
    float m_lifes;
    float m_power;
    float m_defense;
    string m_name;
public:
    Knight(float power, float def, string name) {
        m_name = name;
        m_lifes = 100;
        setDefense(def);
        setPower(power);
    }

    float getLifes() {
        return m_lifes;
    }

    float getPower() {
        return m_power;
    }

    float getDefense() {
        return m_defense;
    }

    string getName() {
        return m_name;
    }

    void fight(Dragon *enemy) {
        float dragonPower = enemy->getPower();
        if (dragonPower > m_defense) {
            // drak je silnejsi, musime znizit zivoty rytierovi
            m_lifes -= dragonPower - m_defense;
        }

        float dragonDefense = enemy->getDefense();
        if (dragonDefense < m_power) {
            // rytier je silnejsi, musisme znizit zivot drakovi
            enemy->reduceLifes(m_power - dragonDefense);
        }
    }


private:
    void setPower(float power) {
        if ((power >= 0) and (power <= 100)) {
            m_power = power;
        } else {
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Knight::setPower", "Power is out of range "
                                              "<0,100>. Set to 0.");
            m_power = 0;
        }
    }

    void setDefense(float def) {
        if ((def >= 0) and (def <= 200)) {
            m_defense = def;
        } else {
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Knight::setDefense", "Defense is out of range "
                                                "<0,200>. Set to 0.");
            m_defense = 0;
        }
    }
};

int main() {
    Dragon *smak = new Dragon(180, 360);
    Knight *artus = new Knight(70, 70, "Artus");

    artus->fight(smak);

    cout << artus->getName() << ": " << artus->getLifes() << endl;
    cout << "Smak: " << smak->getLifes() << endl;

    ErrorLog *log = ErrorLog::getErrorLog();
    cout << log->getErrors();

    artus->fight(smak);

    cout << artus->getName() << ": " << artus->getLifes() << endl;
    cout << "Smak: " << smak->getLifes() << endl;

    delete smak;
    delete (artus);
    return 0;
}












