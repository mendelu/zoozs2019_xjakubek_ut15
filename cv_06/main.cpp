#include <iostream>
#include <array>
#include <vector>

using namespace std;

class Box {
    float m_weight;
    string m_content;
    string m_owner;

public:
    Box(float w, string c, string o) {
        m_weight = w;
        m_content = c;
        m_owner = o;
    }

    string getContent() {
        return m_content;
    }

    string getOwner() {
        return m_owner;
    }
};

class Floor {
    string m_label;
    array<Box *, 10> m_position;
public:
    Floor(string label) {
        m_label = label;
        for (int i = 0; i < m_position.size(); ++i) {
            m_position.at(i) = nullptr;
        }
//        for (auto *&currentPosition:m_position) {
//            currentPosition = nullptr;
//        }
    }

    void saveBox(int position, Box *box) {
        if ((position >= 0) and (position < 10)) {
            // kontrola ci je to obsadene
            if (m_position.at(position) == nullptr) {
                // pozicia je prazdna, mozem pridavat
                m_position.at(position) = box;
            } else {
                cout << "There is box at position " << position << "! \n";
            }
        } else {
            cout << "You are saving out of range!" << endl;
        }
    }

    void removeBox(int position) {
        if ((position >= 0) and (position < 10)) {
            // kontrola ci je to obsadene
            if (m_position.at(position) != nullptr) {
                // pozicia je obsadena, mozme odstranit
                m_position.at(position) = nullptr;
            } else {
                cout << "There is no box at position " << position << "! \n";
            }
        } else {
            cout << "You are saving out of range!" << endl;
        }
    }

    void printInfo() {
        cout << endl << "Floor state: " << endl;
        for (auto *currentPosition:m_position) {
            if (currentPosition != nullptr) {
                cout << currentPosition->getOwner() << " : "
                     << currentPosition->getContent() << endl;
            } else {
                cout << "Position is empty!" << endl;
            }
        }
    }
};

class Store {
    vector<Floor *> m_floors;
public:
    Store() {
        m_floors.push_back(new Floor("Floor n.0"));
    }

    void buildNewFloor() {
        m_floors.push_back(new Floor("Floor n. " + to_string(m_floors.size())));
    }

    void destroyLastFloor() {
        delete (m_floors.at(m_floors.size() - 1));
        m_floors.pop_back();
    }

    void storeBox(int floor, int position, Box *box) {
        // TODO kontrola ci je floor v aktualnom range intervale
        m_floors.at(floor)->saveBox(position, box);
    }

    void removeBox(int floor, int position) {
        // TODO rovnaka kontrola
        m_floors.at(floor)->removeBox(position);
    }

    void printInfo() {
        for (auto *floor:m_floors) {
            floor->printInfo();
        }

//        for (int i = 0; i < m_floors.size(); i++) {
//            m_floors[i]->printInfo();
//        }
    }

    ~Store() {
        for (auto *floor:m_floors) {
            delete (floor);
        }
    }
};

int main() {
    auto *boxik = new Box(100, "lentilky", "Tomas");
    Store *store = new Store();
    store->buildNewFloor();
    store->buildNewFloor();
    store->destroyLastFloor();
    store->storeBox(0, 0, boxik);
    store->printInfo();

//    auto floor1 = new Floor("floor 1");
//    floor1->saveBox(5, boxik);
//    floor1->printInfo();
//
//    // presun z 5 na 4
//    floor1->removeBox(5);
//    floor1->saveBox(4, boxik);
//    floor1->printInfo();

    delete boxik;
//    delete floor1;
    delete (store);
    return 0;
}





