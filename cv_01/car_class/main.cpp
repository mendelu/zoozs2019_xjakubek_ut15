/**
 * Priklad 2:
 * Objekt triedy Auto bude mat atributy spz, rychlost a priznak, ze ci je alebo nie je auto v pohybe.
 * Obsahuje taktiez metody, pre nastavenie SPZ a nastavenie pohybu. Metoda `getSpz()` bude vracat
 * aktualnu spztku. Trieda bude obsahovat metodu pre vypis informacii o aute s nazvom `printInfo`,
 * ktora vypise rozdielny vypis podla toho, ci auto je v pohybe alebo nie.
 */
#include <iostream>

using namespace std;

// Deklaraciu triedy zaciname klucovym slovom `class`, za ktorym nasleduje jej nazov
class Auto {
// do casti public pride vsetko, co ma byt vidiet vo vnutri triedy
public:
    // deklaracia atributov m_spz, m_rychlost a m_pohyb
    string m_spz;
    // nastavenie predvolenej rychlosti na hodnotu 50
    int m_rychlost = 50;
    // datovy typ `bool`, ktory moze nadobudnut len 2 hodnoty:
    // 1. `true` = auto je v pohybe
    // 2. `false` = auto nie je v pohybe (stoji)
    bool m_pohyb;

    /**
    * SET = nastav
    * Klicove slovo `void` povie prekladacu, ze tato metoda nic nevracia. A preto tu
    * mozme robit co chceme (napriklad nastavovat atribut/y). Typicky metoda prijima
    * aspon 1 parameter, ktory je uvedeny v zatvorke.
    */
    void setSpz(string novaSpz) {
        m_spz = novaSpz;
    }

    /**
     * GET = vrat
     * `string` hovori, ze metoda po zavolani vrati retazec.
     */
    string getSpz() {
        return m_spz;
    }

    // SET = nastav
    void setPohyb(bool pohyb) {
        m_pohyb = pohyb;
    }

    /**
     * print = tiskni/vypis
     * Metoda, ktora nic nevracia (navratovy typ je `void`) a nic nenastavuje (nie su
     * ziadne parametre v zatvorke).
     */
    void printInfo() {
        if (m_pohyb == true) {
            // vyraz v podmienke je pravdivy (auto je v pohybe), program pokracuje sem

            cout << "Auto s SPZ " << getSpz() << " ide rychlostou: " << m_rychlost;
        } else {
            // vyraz v podmienke nie je pravdivy (auto nie je v pohybe), program pokracuje sem

            cout << "Auto s SPZ " << m_spz << " stoji na mieste.";
        }

        // prechod na novy riadok
        cout << endl;
    }
};

// hlavna funkcia programu `main()`, ktora je mimo triedy `Auto`
int main() {
    // deklaracia premmenej `ferrari`, typom je trieda objektu
    Auto *ferrari;

    // vytvorenie noveho objektu pomocou klucoveho slova `new`
    ferrari = new Auto();

    // nastavenie atributu spz
    ferrari->setSpz("Brno123");
    // vypis informacii o objekte
    ferrari->printInfo();

    // nastavenie atributu `pohyb` na `true` - auto bude v pohybe
    ferrari->setPohyb(true);
    // vypis informacii o objekte
    ferrari->printInfo();

    // nastavenie atributu `pohyb` na `false` - auto zastavi
    ferrari->setPohyb(false);
    // vypis informacii o objekte
    ferrari->printInfo();

    // ukoncenie aplikacie
    return 0;
}