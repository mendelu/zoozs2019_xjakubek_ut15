//
// Created by xjakubek on 19.11.2019.
//

#include "Student.h"

Student::Student(std::string name, std::string id, int semester,
                 float average) : Person(name, id) {
    m_average = average;
    m_semester = semester;
}

void Student::addNextSemester() {
    m_semester += 1;
}

void Student::printInfo() {
    std::cout << "Student: " << std::endl;
    std::cout << "Name: " << m_name << std::endl;
    std::cout << "ID: " << m_id << std::endl;
    std::cout << "Average: " << m_average << std::endl;
    std::cout << "Semester: " << m_semester << std::endl;
}
