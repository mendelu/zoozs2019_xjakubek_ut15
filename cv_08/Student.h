//
// Created by xjakubek on 19.11.2019.
//

#ifndef CV_08_STUDENT_H
#define CV_08_STUDENT_H

#include "Person.h"

class Student : public Person {
    int m_semester;
    float m_average;
public:
    Student(std::string name, std::string id, int semester, float average);

    void addNextSemester();

    void printInfo();
};


#endif //CV_08_STUDENT_H
