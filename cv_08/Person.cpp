//
// Created by xjakubek on 19.11.2019.
//

#include "Person.h"

Person::Person(std::string name, std::string id) {
    m_name = name;
    m_id = id;
}

std::string Person::getId() {
    return m_id;
}

std::string Person::getName() {
    return m_name;
}

void Person::setId(std::string id) {
    m_id = id;
}

void Person::setName(std::string name) {
    m_name = name;
}

void Person::printInfo() {
    std::cout << "Osoba s menom " << getName() << std::endl;
}