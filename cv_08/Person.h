//
// Created by xjakubek on 19.11.2019.
//

#ifndef CV_08_PERSON_H
#define CV_08_PERSON_H

#include <iostream>

class Person {
protected:
    std::string m_name;
    std::string m_id;
public:
    Person(std::string name, std::string id);

    std::string getName();

    std::string getId();

    void setName(std::string name);

    void setId(std::string id);

    void printInfo();
};


#endif //CV_08_PERSON_H
