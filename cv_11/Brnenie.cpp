//
// Created by xjakubek on 17.12.2019.
//

#include "Brnenie.h"

Brnenie::Brnenie(int vaha, int odolnost) {
    m_vaha = vaha;
    m_odolost = odolnost;
}

void Brnenie::printInfo() {
    std::cout << "brnenie: " << std::endl;
    std::cout << " - vaha brnenia: " << m_vaha << std::endl;
    std::cout << " - bonus utoku: " << getBonusUtoku() << std::endl;
    std::cout << " - bonus obrany: " << getBonusObrany() << std::endl;
}


