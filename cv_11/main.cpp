#include <iostream>

#include "RytierDirector.h"
#include "LahkoOddenecBuilder.h"
#include "TazkoOddenecBuilder.h"

int main() {
    auto *lob = new LahkoOddenecBuilder;
    auto *director = new RytierDirector(lob);

    auto *peter = director->createRytier("Peter", 100, 50, 10, "XXL");
    peter->printInfo();

    director->setRytierBuilder(new TazkoOddenecBuilder);
    peter = director->createRytier("Ondra", 60, 69, 70, "S");
    peter->printInfo();

    delete lob;
    delete director;
    delete peter;
    return 0;
}