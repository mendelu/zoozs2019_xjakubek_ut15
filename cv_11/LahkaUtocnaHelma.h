//
// Created by xjakubek on 17.12.2019.
//

#ifndef CV_11_LAHKAUTOCNAHELMA_H
#define CV_11_LAHKAUTOCNAHELMA_H

#include "Helma.h"

class LahkaUtocnaHelma : public Helma {
public:
    LahkaUtocnaHelma(std::string velkost);

    int getBonusObrany() override;

    void printInfo() override;
};


#endif //CV_11_LAHKAUTOCNAHELMA_H
