//
// Created by xjakubek on 17.12.2019.
//

#ifndef CV_11_HELMA_H
#define CV_11_HELMA_H

#include <iostream>

class Helma {
private:
    std::string m_velkost;
public:
    Helma(std::string velkost);

    virtual int getBonusObrany() = 0;

    virtual void printInfo();
};


#endif //CV_11_HELMA_H
