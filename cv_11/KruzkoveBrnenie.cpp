//
// Created by xjakubek on 17.12.2019.
//

#include "KruzkoveBrnenie.h"

KruzkoveBrnenie::KruzkoveBrnenie(
        int vaha,
        int odolnost,
        int ohybnost) : Brnenie(vaha, odolnost) {

    m_ohybnost = ohybnost;
}

int KruzkoveBrnenie::getBonusUtoku() {
    return m_odolost / 4 + m_ohybnost;
}

int KruzkoveBrnenie::getBonusObrany() {
    return m_odolost + m_ohybnost / 4;
}

void KruzkoveBrnenie::printInfo() {
    Brnenie::printInfo();
    std::cout << " - typ: kruzkove brnenie" << std::endl;
}
