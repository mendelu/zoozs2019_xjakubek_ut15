//
// Created by xjakubek on 17.12.2019.
//

#include "LahkoOddenecBuilder.h"

void LahkoOddenecBuilder::buildBrnenie(int vaha, int odolnost) {
    m_rytier->setBrnenie(
            new KruzkoveBrnenie(vaha, odolnost, 20));
}

void LahkoOddenecBuilder::buildHelma(std::string velkost) {
    m_rytier->setHelma(new LahkaUtocnaHelma(velkost));
}