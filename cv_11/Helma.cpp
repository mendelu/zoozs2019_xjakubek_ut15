//
// Created by xjakubek on 17.12.2019.
//

#include "Helma.h"

Helma::Helma(std::string velkost) {
    m_velkost = velkost;
}

void Helma::printInfo() {
    std::cout << "Helma: " << std::endl;
    std::cout << " - velkost helmy: " <<
              m_velkost << std::endl;
    std::cout << " - bonus obrany: " <<
              getBonusObrany() << std::endl;
}