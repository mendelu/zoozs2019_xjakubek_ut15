//
// Created by xjakubek on 17.12.2019.
//

#include "PlatoveBrnenie.h"

PlatoveBrnenie::PlatoveBrnenie(
        int vaha,
        int odolnost) : Brnenie(vaha, odolnost) {}

int PlatoveBrnenie::getBonusUtoku() {
    return m_odolost / 4;
}

int PlatoveBrnenie::getBonusObrany() {
    return m_odolost;
}

void PlatoveBrnenie::printInfo() {
    Brnenie::printInfo();
    std::cout << " - typ: platove brnenie" << std::endl;
}