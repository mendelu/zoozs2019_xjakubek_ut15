//
// Created by xjakubek on 17.12.2019.
//

#include "RytierDirector.h"

RytierDirector::RytierDirector(RytierBuilder *builder) {
    m_rytierBuilder = builder;
}

void RytierDirector::setRytierBuilder(RytierBuilder *builder) {
    m_rytierBuilder = builder;
}

Rytier *RytierDirector::createRytier(
        std::string meno, int sila,
        int vahaBrnenia, int odolnostBrnenia,
        std::string velkostHelmy) {

    m_rytierBuilder->createRytier(meno, sila);
    m_rytierBuilder->buildBrnenie(vahaBrnenia, odolnostBrnenia);
    m_rytierBuilder->buildHelma(velkostHelmy);

    return m_rytierBuilder->getRytier();
}