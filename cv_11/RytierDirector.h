//
// Created by xjakubek on 17.12.2019.
//

#ifndef CV_11_RYTIERDIRECTOR_H
#define CV_11_RYTIERDIRECTOR_H

#include "RytierBuilder.h"

class RytierDirector {
private:
    RytierBuilder *m_rytierBuilder;
public:
    RytierDirector(RytierBuilder *builder);

    void setRytierBuilder(RytierBuilder *builder);

    Rytier *createRytier(
            std::string meno, int sila,
            int vahaBrnenia, int odolnostBrnenia,
            std::string velkostHelmy);
};


#endif //CV_11_RYTIERDIRECTOR_H
