//
// Created by xjakubek on 17.12.2019.
//

#ifndef CV_11_TAZKOOBRANNAHELMA_H
#define CV_11_TAZKOOBRANNAHELMA_H

#include "Helma.h"

class TazkoObrannaHelma : public Helma {
private:
    int m_bonusObrany;
public:
    TazkoObrannaHelma(std::string velkost, int bonusObrany);

    int getBonusObrany() override;

    void printInfo() override;
};


#endif //CV_11_TAZKOOBRANNAHELMA_H
