//
// Created by xjakubek on 17.12.2019.
//

#include "TazkoOddenecBuilder.h"

void TazkoOddenecBuilder::buildHelma(std::string velkost) {
    m_rytier->setHelma(new TazkoObrannaHelma(velkost, 100));
}

void TazkoOddenecBuilder::buildBrnenie(int vaha, int odolnost) {
    m_rytier->setBrnenie(new PlatoveBrnenie(vaha, odolnost));
}
