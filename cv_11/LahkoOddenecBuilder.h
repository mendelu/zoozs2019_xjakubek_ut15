//
// Created by xjakubek on 17.12.2019.
//

#ifndef CV_11_LAHKOODDENECBUILDER_H
#define CV_11_LAHKOODDENECBUILDER_H

#include "RytierBuilder.h"
#include "KruzkoveBrnenie.h"
#include "LahkaUtocnaHelma.h"

class LahkoOddenecBuilder: public RytierBuilder {
public:
    void buildBrnenie(int vaha, int odolnost) override;

    void buildHelma(std::string velkost) override;
};


#endif //CV_11_LAHKOODDENECBUILDER_H
