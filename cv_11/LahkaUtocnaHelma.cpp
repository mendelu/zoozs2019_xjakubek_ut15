//
// Created by xjakubek on 17.12.2019.
//

#include "LahkaUtocnaHelma.h"

LahkaUtocnaHelma::LahkaUtocnaHelma(std::string velkost) :
        Helma(velkost) {
    // nemam ziadne dalsie parametre, ktore je potrebne nastavit
}

int LahkaUtocnaHelma::getBonusObrany() {
    return 0;
}

void LahkaUtocnaHelma::printInfo() {
    Helma::printInfo();
    std::cout << " - typ: lahka utocna helma" << std::endl;
}
