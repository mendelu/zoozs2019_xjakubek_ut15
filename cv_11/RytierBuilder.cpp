//
// Created by xjakubek on 17.12.2019.
//

#include "RytierBuilder.h"

Rytier *RytierBuilder::createRytier(std::string meno, int sila) {
    m_rytier = new Rytier(meno, sila);
    return m_rytier;
}

Rytier *RytierBuilder::getRytier() {
    return m_rytier;
}