//
// Created by xjakubek on 03.12.2019.
//

#ifndef CV_10_STUDYTYPE_H
#define CV_10_STUDYTYPE_H

enum class StudyType {
    Bc, Mgr, Phd
};

#endif //CV_10_STUDYTYPE_H
