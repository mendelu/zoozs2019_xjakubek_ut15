//
// Created by xjakubek on 03.12.2019.
//

#ifndef CV_10_STUDENT_H
#define CV_10_STUDENT_H

#include <iostream>
#include "StudyType.h"

using namespace std;

class Student {
    string m_name;
    float m_scholarshipPerYear;
    int m_standardStudyLength;
    bool m_mealDiscount;
    Student(string name, float scholarship, int studyLength,
            bool discount);
public:
    static Student* createStudent(string name, StudyType type);

    string getName();
    float getScholarshipPerYear();
    int getStandardStudyLength();
    bool getMealDiscount();
};


#endif //CV_10_STUDENT_H
