//
// Created by xjakubek on 03.12.2019.
//

#ifndef CV_10_MGRFACTORY_H
#define CV_10_MGRFACTORY_H

#include "StudyFactory.h"

class MgrFactory : public StudyFactory {
public:
    Student *createStudent(string name);
    Course* createCourse(string name, int credits);
};


#endif //CV_10_MGRFACTORY_H
