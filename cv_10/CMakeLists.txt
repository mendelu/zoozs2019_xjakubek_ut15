cmake_minimum_required(VERSION 3.14)
project(cv_10)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv_10 main.cpp Student.cpp Student.h StudyFactory.h BcFactory.cpp BcFactory.h MgrFactory.cpp MgrFactory.h Course.cpp Course.h StudyType.h)