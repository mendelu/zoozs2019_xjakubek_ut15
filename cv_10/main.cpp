#include <iostream>
#include "StudyFactory.h"
#include "BcFactory.h"
#include "MgrFactory.h"

int main() {
//    Student *bc = new Student("Tomas", 1000, 3, true);
//    Student *mgr = new Student("John", 2000, 2, true);
//    Student *phd = new Student("Filip", 0, 3, false);

//    Student *bc = Student::createStudent("Tomas", StudyType::Bc);
//    Student *mgr = Student::createStudent("John", StudyType::Mgr);
//    Student *phd = Student::createStudent("Filip", StudyType::Phd);

    StudyFactory *factory;

    int type;
    cout << "Akeho chces studenta? BC=1, MGR=2: \t";
    cin >> type;

    if (type == 1) {
        factory = new BcFactory();
    } else if (type == 2) {
        factory = new MgrFactory();
    } else {
        cout << "Zly vstup!" << endl;
        return 0;
    }

    auto *someStudent = factory->createStudent("Tomas");
    auto *someCourse = factory->createCourse("ZOO", 5);

    cout << "Student scholarship: " <<
         someStudent->getScholarshipPerYear() << endl;
    cout << "Course name: " << someCourse->getName() << endl;

    delete factory;
    return 0;
}