#include <iostream>

using namespace std;

class Developer {
private:
    float m_baseSalary;
    float m_bonus;
    float m_workHours;
    float m_bugDeduction;
    int m_bugs;
    string m_name;
public:
    Developer(string name, float salary) {
        m_name = name;
        setBaseSalary(salary);
        m_bonus = 500;
        m_bugDeduction = 1000;
        m_workHours = 0.0;
        m_bugs = 0;
    }

    Developer(string name, float salary, float bonus, float deduc) {
        m_name = name;
        setBaseSalary(salary);
        m_bonus = bonus;
        m_bugDeduction = deduc;
        m_workHours = 0.0;
        m_bugs = 0;
    }

    void setBaseSalary(float salary) {
        if (salary > 10000) {
            m_baseSalary = salary;
        } else {
            cout << "Minimal salary is 10001" << endl;
        }
    }

    void addBug() {
        m_bugs++;
        // m_bugs += 1;
        // m_bugs = m_bugs + 1;
    }

    float calculateSalary() {
        float result = 0;

        // pridam zakladny plat
        result += m_baseSalary;

        if (m_workHours > 40) {
            // ti co odrobili viac ako 40 hodin
            result += (m_workHours - 40) * m_bonus;
        }

        // odcitat pokutu za bugy
        result -= m_bugs * m_bugDeduction;

        return result;
    }

    void resetMonth() {
        m_bugs = 0;
        m_workHours = 0;
    }

    void setWorkHours(float hours) {
        m_workHours = hours;
    }

    void printInfo() {
        // TODO
    }
};


int main() {
    Developer *tomas;

    tomas = new Developer("Tomas", 20000, 800, 1200);

    tomas->addBug();
    tomas->addBug();
    tomas->addBug();
    tomas->setWorkHours(50);

    float totalSalary = tomas->calculateSalary();
    cout << "Month 1: " << totalSalary << endl;

    // novy mesiac
    tomas->resetMonth();
    tomas->setBaseSalary(25000);
    tomas->addBug();
    tomas->setWorkHours(30);

    totalSalary = tomas->calculateSalary();
    cout << "Month 2: " << totalSalary << endl;

    delete(tomas);
    return 0;
}











