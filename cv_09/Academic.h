//
// Created by xjakubek on 26.11.2019.
//

#ifndef CV_09_ACADEMIC_H
#define CV_09_ACADEMIC_H

#include "JobPosition.h"

class Academic : public JobPosition {
public:
    float getSalary(int years, int education);

    int getRestHoliday(int usedHoliday);
};


#endif //CV_09_ACADEMIC_H
