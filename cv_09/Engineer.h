//
// Created by xjakubek on 26.11.2019.
//

#ifndef CV_09_ENGINEER_H
#define CV_09_ENGINEER_H

#include "JobPosition.h"

class Engineer: public JobPosition {
public:
    float getSalary(int years, int education);

    int getRestHoliday(int usedHoliday);
};


#endif //CV_09_ENGINEER_H
