//
// Created by xjakubek on 26.11.2019.
//

#include "Engineer.h"

int Engineer::getRestHoliday(int usedHoliday) {
    const int maxHolidays = 30;
    return maxHolidays - usedHoliday;
}

float Engineer::getSalary(int years, int education) {
    const int baseSalary = 30000;
    const int yearsBonus = 1000;

    return baseSalary + years * yearsBonus;
}