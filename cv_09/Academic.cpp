//
// Created by xjakubek on 26.11.2019.
//

#include "Academic.h"

float Academic::getSalary(int years, int education) {
    const int baseSalary = 40000;
    const int educationBonus = 5000;

    return baseSalary + education * educationBonus;
}

int Academic::getRestHoliday(int usedHoliday) {
    const int maxHolidays = 50;

    return maxHolidays - usedHoliday;
}
