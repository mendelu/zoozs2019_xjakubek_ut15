//
// Created by xjakubek on 26.11.2019.
//

#include "Employee.h"

Employee::Employee(int years, int education,
                   PositionType positionType) {
    m_education = education;
    m_years = years;
    m_usedHoliday = 0;
    m_position = nullptr;
    changeJobPosition(positionType);
}

void Employee::addNewHoliday(int newHoliday) {
    m_usedHoliday += newHoliday;
}

void Employee::printInfo() {
    std::cout << "Zvysok dovolenky: " <<
              m_position->getRestHoliday(m_usedHoliday) << std::endl;
    std::cout << "Plat: " <<
              m_position->getSalary(m_years, m_education) << std::endl;
}

void Employee::changeJobPosition(PositionType newPosition) {
    if (m_position != nullptr) {
        delete m_position;
    }

    if (newPosition == PositionType::ACADEMIC) {
        m_position = new Academic();
    } else {
        m_position = new Engineer();
    }
}
