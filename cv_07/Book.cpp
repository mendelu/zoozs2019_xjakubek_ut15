//
// Created by xjakubek on 12.11.2019.
//

#include "Book.h"

int Book::s_instances = 0;

Book::Book(string author, string title) {
    m_author = author;
    m_title = title;
    s_instances += 1;
    m_id = s_instances;
}

string Book::getAuthor() {
    return m_author;
}

int Book::getId() {
    return m_id;
}

void Book::printInfo() {
    cout << "Book info: " << endl;
    cout << " - author: " << m_author << endl;
    cout << " - title: " << m_title << endl;
    cout << " - id: " << m_id << endl;
}