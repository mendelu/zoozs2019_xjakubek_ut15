//
// Created by xjakubek on 12.11.2019.
//

#ifndef CV_07_LIBRARY_H
#define CV_07_LIBRARY_H

#include <vector>
#include "iostream"
#include "Book.h"

using namespace std;

class Library {
    static vector<Book *> s_books;

    Library() = default;
//    Library(){};

public:

    static void createBook(string author, string title);

    static void addBook(Book *newBook);

    static void removeBook(int id);

    static void printInfo();

    static vector<Book*> searchBooksByAuthor(string author);

};


#endif //CV_07_LIBRARY_H
