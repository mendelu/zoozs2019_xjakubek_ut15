#include <iostream>

#include "Library.h"

int main() {
    Library::createBook("Jozo", "Stale neviem ZOO");
    Library::createBook("Fero", "Budeme plakat");
    Library::createBook("Jozo", "Radsej sa to naucim");

    // vymazem Fera
    Library::removeBook(2);
    Library::printInfo();

    return 0;
}