//
// Created by xjakubek on 12.11.2019.
//

#ifndef CV_07_BOOK_H
#define CV_07_BOOK_H

#include <iostream>

using namespace std;

class Book {
    string m_author;
    string m_title;
    int m_id;
    static int s_instances;
public:
    Book(string author, string title);

    int getId();

    void printInfo();

    string getAuthor();
};


#endif //CV_07_BOOK_H
